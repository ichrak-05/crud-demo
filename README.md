# CRUD-Demo 



## CRUD Application

### **1-Introduction**:

I developed a straightforward **CRUD** application for an entity named `"Article"` as part of my PFE internship. The primary functionalities include **creating** (C), **reading** (R), **updating** (U), and **deleting** (D) instances of the entity. This application serves as an initiative during my PFE internship, demonstrating the fundamental operations for managing articles.

**Technologies Utilisées:**

- **Backend:** Spring Boot, Java 17, Maven, Mockito, Keycloak, MapStruct.
- **Frontend:** Angular.
- **DataBase:** MongoDB.
- **Authentification:** JWT (JSON Web Tokens) via Keycloak.

### **2-Architecture**:

To implement the CRUD application, I adhered to the Model-View-Controller (MVC) architecture. The project structure is organized into several packages, each serving a distinct purpose.

**Entity Package:**
In this package, I created the 'Entity' package, housing the core entity `"Article"` along with Data Transfer Objects (DTOs) like `"GetArticleDTO"` and `"SaveArticleDTO".` This compartmentalization ensures a clear separation between the entity and the data being transferred between layers.

To facilitate the mapping between the ``Article`` entity and its corresponding DTOs, the application utilizes `MapStruct`. The ``ArticleMapper`` interface, annotated with ``@Mapper``, encapsulates the mapping logic:

```java
@Mapper
public interface ArticleMapper {

    @Mapping(target = "id", ignore = true)
    ArticleDTO articleToDTO(Article article);

    @Mapping(target = "id", ignore = true)
    AddArticleDTO articleToAddArticleDTO(Article article);

    // Additional mapping methods as needed
}
```

**Repository Package:**
Within the `'Repository'` package, I established the `"ArticleRepository"` interface. This interface extends the `"MongoRepository"` class and serves as the entry point for interacting with the underlying MongoDB database. It provides convenient methods for CRUD operations on the `"Article"` entity.

**Service Package:**
The `'Service'` package contains the business logic layer. Here, I defined the `"ArticleService"` interface and its corresponding implementation class, `"ArticleServiceImpl"`. This separation of concerns ensures that the application's services are decoupled from the underlying data access logic, promoting maintainability and scalability.

**Controller Package:** 

In the `'Controller'` package, I implemented the primary APIs responsible for handling various **HTTP** requests, including **GET**, **POST**, **PUT**, and **DELETE**. This package plays a pivotal role in the Model-View-Controller (MVC) architecture, serving as the interface between the client (frontend or external system) and the backend services.

 

**Exceptions Package:**
I introduced a dedicated `'Exceptions'` package to handle exceptional scenarios. Within this package, I implemented two custom exceptions: `"EntityNotFoundException"` for cases where a requested entity is not found, and `"EntityAlreadyExistException"` for situations where an attempt is made to create an entity that already exists. These exception classes facilitate providing meaningful error messages and handling exceptional cases gracefully within the application.

### **3. Configuration:**

In the `'Configuration'` section, I'll provide an overview of how the application is configured, covering key aspects such as development environment setup, database configuration, and any authentication-related settings.

**Development Environment Configuration:**
For local development, the application requires certain configurations to ensure a smooth development experience. This includes setting up the necessary development tools, **IDE** configurations (**Spring Tools**), and ensuring compatibility with the specified **Java** version (**Java 17**).

**Database Configuration:**
The application utilizes **MongoDB** as its underlying database. MongoDB-specific configurations, such as connection details, **database name**, and any additional settings, are specified in the `‘application.properties'` file or an equivalent configuration file. 

**Authentication Configuration:**
The application incorporates Keycloak for authentication, and **JWT** (JSON Web Tokens) are used for secure communication. 

**Maven Configuration:**
**Maven** is employed for project management, dependency resolution, and building the application. The `'pom.xml'` file contains project-specific configurations, including **dependencies**, **plugins**, and **build settings**. Maven simplifies the build process, allowing for easy project compilation, testing, and packaging.

**Testing Configuration:**
The application incorporates unit testing using the **Mockito** library. The testing configuration involves specifying dependencies for testing in the Maven build file and ensuring that the testing environment is set up appropriately. This ensures that unit tests are executed effectively, validating the functionality of individual components in isolation.

### **4- Frontend (Angular)**

In the 'Frontend (Angular)' section, I'll elaborate on the Angular implementation of the CRUD application. The frontend is structured around four key components, each serving a specific purpose in the user interface, and two principal services responsible for authentication and CRUD operations.

**Components:**

1. **AllArticles Component:**
    - The `'AllArticles'` component is designed to display a list of all articles. It utilizes the CRUD service to fetch data from the backend and presents it in a user-friendly manner. This component may include features like pagination, sorting, and filtering for enhanced user interaction.
2. **Login Component:**
    - The `'Login'` component serves as the user interface for authentication. It provides a form for users to enter their credentials and submit them to the authentication service. Upon successful authentication, users may gain access to the CRUD functionalities.
3. **New Article Component:**
    - The `'New Article'` component facilitates the creation of a new article. It includes a form where users can input the necessary details for a new article. When submitted, the component interacts with the CRUD service to send the data to the backend for storage.
4. **Update Article Component:**
    - The `'Update Article'` component is responsible for modifying existing articles. It presents a form with pre-filled data, allowing users to make changes. Similar to the 'New Article' component, it communicates with the CRUD service to update the backend with the modified article details.
5. **Services:**

**Authentication Service:**

- The authentication service manages user authentication and authorization. It communicates with the backend to validate user credentials, obtain authentication tokens (JWT), and handle user sessions. The `'Login'` component interacts with this service for user authentication.

**CRUD Service:**

- The CRUD service is a fundamental service responsible for handling Create, Read, Update, and Delete operations on articles. The `'AllArticles,'` `'New Article'` and `'Update Article'` components leverage this service to interact with the backend, ensuring seamless communication for managing articles.
