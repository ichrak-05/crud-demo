package com.example.demo.Exceptions;

public class EntityNotFoundException extends Exception{
    public EntityNotFoundException(String msg) {
        super(msg);
    }
}
