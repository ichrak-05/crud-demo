package com.example.demo.Exceptions;

public class EntityAlreadyExistException extends Exception {

    public EntityAlreadyExistException(String msg) {
        super(msg);
    }
}

