package com.example.demo.Controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Entities.AddArticleDTO;
import com.example.demo.Entities.Article;
import com.example.demo.Entities.GetArticleDTO;
import com.example.demo.Exceptions.EntityAlreadyExistException;
import com.example.demo.Exceptions.EntityNotFoundException;
import com.example.demo.Service.ArticleService;

@RestController

@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
@RequestMapping("/articles")
public class ArticleController {

	@Autowired
	 private  ArticleService articleService;
	 
	 
	@GetMapping("/all")
    public List<Article> getAllArticles() {
        return articleService.getAllArticles();
    }

	@GetMapping("/byid/{id}")
	public Article getArticleById(@PathVariable("id") String id) throws EntityNotFoundException {
	    return articleService.getArticleById(id);
	}


    
	@PostMapping("/add")
	public Article createArticle(@RequestBody AddArticleDTO article) throws EntityAlreadyExistException {
	    // Générer un nouvel ID
	    String newId = UUID.randomUUID().toString();

	    // Créer un nouvel article à partir des données de AddArticleDTO
	    Article newArticle = new Article(
	            newId,
	            article.getName(),
	            article.getDescription(),
	            article.getIsAvailable(),
	            article.getInStock()
	    );

	    // Enregistrez l'article dans le service
	    Article savedArticle = articleService.saveArticle(newArticle);

	    // Retournez l'article enregistré
	    return savedArticle;
	}

	
    @PutMapping("/update/{id}")
    public Article updateArticle(@PathVariable("id") String id, @RequestBody Article updatedArticle) throws EntityNotFoundException, EntityAlreadyExistException {
        // Vérifiez d'abord si l'article avec l'ID spécifié existe
    	Article existingArticle = articleService.getArticleById(id);
        if (existingArticle != null) {
            
            return articleService.UpdateArticle(id,updatedArticle);
        } else {
            return null; // Ou lancez une exception appropriée, l'article n'est pas disponible
        }
    }

    @DeleteMapping("/delete/{id}") 
    public void deleteArticle(@PathVariable("id") String id) throws EntityNotFoundException {
        articleService.deleteArticle(id);
    }
}



