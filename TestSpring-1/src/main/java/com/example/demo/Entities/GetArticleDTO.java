package com.example.demo.Entities;

public class GetArticleDTO {

    private String name;
    private String description;
    private Boolean isAvailable;
    private Integer inStock;

    public GetArticleDTO(String name, String description, Boolean isAvailable, Integer inStock) {
        this.name = name;
        this.description = description;
        this.isAvailable = isAvailable;
        this.inStock = inStock;
    }
    
    public GetArticleDTO()
    {
    	
    }

    // Constructeur pour mapper un objet de type Article à un objet de type GetArticleDTO
    public GetArticleDTO(Article article) {
        this(article.getName(), article.getDescription(), article.getIsAvailable(), article.getInStock());
    }

    // Méthodes getters
    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Boolean getIsAvailable() {
        return isAvailable;
    }

    public Integer getInStock() {
        return inStock;
    }
}

/*
public record GetArticleDTO(String name, String description,
		Boolean isAvailable, Integer inStock) {

	
	// methode pour mapper un objet de type article à un objet de type GetArticleDTO
	 public GetArticleDTO(Article article) {
	        this(article.getName(), article.getDescription(), article.getIsAvailable(),
	                article.getInStock());
	    }
}
*/