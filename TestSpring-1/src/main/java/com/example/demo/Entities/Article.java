package com.example.demo.Entities;

import org.springframework.data.mongodb.core.mapping.Document;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;

import org.springframework.data.annotation.Id;

@Document(collection = "Articles")
public class Article {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	
	private String Name;
	private String Description;
	private Boolean isAvailable;
	private Integer InStock;
	
	
	public Article(String id, String name, String description, Boolean isAvailable, Integer inStock) {
		super();
		id = id;
		Name = name;
		Description = description;
		this.isAvailable = isAvailable;
		InStock = inStock;
	}

	// Constructeur par défaut
    public Article() {
    }

	public String getId() {
		return id;
	}



	public String getName() {
		return Name;
	}


	public void setName(String name) {
		Name = name;
	}

	public void setId(String id) {
		id = id;
	}

	public String getDescription() {
		return Description;
	}


	public void setDescription(String description) {
		Description = description;
	}


	public Boolean getIsAvailable() {
		return isAvailable;
	}


	public void setIsAvailable(Boolean isAvailable) {
		this.isAvailable = isAvailable;
	}


	public Integer getInStock() {
		return InStock;
	}


	public void setInStock(Integer inStock) {
		InStock = inStock;
	}
	
	
	

}
