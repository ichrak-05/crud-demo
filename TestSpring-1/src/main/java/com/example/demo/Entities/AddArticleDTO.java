package com.example.demo.Entities;

import java.util.UUID;

import org.springframework.data.annotation.Id;

public class AddArticleDTO {

    private String id;
    private String name;
    private String description;
    private Boolean isAvailable;
    private Integer inStock;

    public AddArticleDTO(String id, String name, String description, Boolean isAvailable, Integer inStock) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.isAvailable = isAvailable;
        this.inStock = inStock;
    }
    
    public AddArticleDTO() {
       
    }

    public AddArticleDTO(Article article) {
        this(article.getId(), article.getName(), article.getDescription(), article.getIsAvailable(),
                article.getInStock());
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsAvailable() {
		return isAvailable;
	}

	public void setIsAvailable(Boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

	public Integer getInStock() {
		return inStock;
	}

	public void setInStock(Integer inStock) {
		this.inStock = inStock;
	}

   

}










/*
 * public record AddArticleDTO(String id, String name, String description,
		Boolean isAvailable, Integer inStock) {

	 public AddArticleDTO(Article article) {
	        this(article.getId(), article.getName(), article.getDescription(), article.getIsAvailable(),
	                article.getInStock());
	    }
	
}
 
 */
