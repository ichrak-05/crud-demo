package com.example.demo.Repository;


import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.Entities.Article;

@Repository
public interface ArticleRepository extends MongoRepository<Article, String> {
    
	@Query("{ '_id': ?0 }")
	Article findArticleById(String id);


	
}



