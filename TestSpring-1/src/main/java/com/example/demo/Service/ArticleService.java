package com.example.demo.Service;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Service;


import com.example.demo.Entities.Article;

import com.example.demo.Exceptions.EntityAlreadyExistException;
import com.example.demo.Exceptions.EntityNotFoundException;

@Service
public interface ArticleService {

	
	public List<Article> getAllArticles() ;
	public Article getArticleById(String id) throws EntityNotFoundException ;
	public Article saveArticle(Article article1) throws EntityAlreadyExistException ;
	public void deleteArticle(String id) throws EntityNotFoundException;
	public Article UpdateArticle(String id, Article article) throws EntityNotFoundException;
	 
}
