package com.example.demo.Service;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;


import com.example.demo.Entities.Article;

import com.example.demo.Exceptions.EntityAlreadyExistException;
import com.example.demo.Exceptions.EntityNotFoundException;
import com.example.demo.Repository.ArticleRepository;
import com.mongodb.DuplicateKeyException;

import jakarta.persistence.EntityExistsException;

@Service
public class ArticleSericeImpl implements ArticleService {

	@Autowired
	private  ArticleRepository articleRepository;
	
	
	@Override
	public List<Article> getAllArticles()  {
	    List<Article> articles = articleRepository.findAll();
	   // List<GetArticleDTO> articleDTOs = new ArrayList<>();

	  /*  if( articleDTOs != null)
	    	
	    {
		    for (Article article : articles) {
		        GetArticleDTO articleDTO = new GetArticleDTO(
		                article.getName(),
		                article.getDescription(),
		                article.getIsAvailable(),
		                article.getInStock()
		        );
	
		        articleDTOs.add(articleDTO);
		    }
	    }
	   
*/
	    return articles;
	}


	@Override
	public Article getArticleById(String id) throws EntityNotFoundException {
	    Optional<Article> optionalArticle = articleRepository.findById(id);

	    if (optionalArticle.isPresent()) {
	        return optionalArticle.get();
	    } else {
	        throw new EntityNotFoundException("Cannot find Article with id: " + id);
	    }
	}

	

	@Override
	
	public Article saveArticle(Article article) throws EntityAlreadyExistException {
		
	
	return  articleRepository.save(article);
	
	}
	
	/*
	public AddArticleDTO saveArticle(Article article) throws EntityAlreadyExistException {
	    if (article.getId() == null || !articleRepository.existsById(article.getId())) {
	        // Générer un nouvel id s'il est null ou n'existe pas dans la base de données
	        if (article.getId() == null) {
	            article.setId(generateRandomString());
	        }

	        try {
	            articleRepository.save(article);
	            return new AddArticleDTO(article);
	        } catch (DuplicateKeyException e) {
	            // L'ID existe déjà dans la base de données
	            throw new EntityAlreadyExistException("Article already exists");
	        }
	    }

	    // Lancer une exception si l'article a déjà un id existant dans la base de données
	    throw new EntityAlreadyExistException("Article already has an id");
	}
	
	private String generateRandomString() {
	    // Utilisez UUID.randomUUID().toString() pour générer une chaîne aléatoire
	    return UUID.randomUUID().toString();
	}


*/
	
	// Méthode pour générer une chaîne aléatoire (à adapter selon vos besoins)
	private String generateRandomString(int length) {
        if (length <= 0) {
            throw new IllegalArgumentException("La longueur doit être supérieure à 0");
        }

        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        StringBuilder sb = new StringBuilder(length);
        SecureRandom random = new SecureRandom();

        for (int i = 0; i < length; i++) {
            int randomIndex = random.nextInt(characters.length());
            char randomChar = characters.charAt(randomIndex);
            sb.append(randomChar);
        }

        return sb.toString();
    }

	@Override
	public void deleteArticle(String id) throws EntityNotFoundException {
	    // TODO Auto-generated method stub
	    if (articleRepository.findArticleById(id) != null) {
	        articleRepository.deleteById(id);
	    } else {
	        // Exception Entity not found
	        throw new EntityNotFoundException("Article not found");
	    }
	}


	@Override
	public Article UpdateArticle(String id, Article updatedArticle) throws EntityNotFoundException {
	    // Vérifiez d'abord si l'article avec l'ID spécifié existe
	    Article existingArticle = articleRepository.findById(id)
	            .orElseThrow(() -> new EntityNotFoundException("Article not found"));

	    // Mettez à jour les champs de l'article existant avec les nouvelles valeurs
	    existingArticle.setName(updatedArticle.getName());
	    existingArticle.setDescription(updatedArticle.getDescription());
	    existingArticle.setIsAvailable(updatedArticle.getIsAvailable());
	    existingArticle.setInStock(updatedArticle.getInStock());

	    // Enregistrez l'article mis à jour dans la base de données en préservant l'ID existant
	    return articleRepository.save(existingArticle);
	}



}
