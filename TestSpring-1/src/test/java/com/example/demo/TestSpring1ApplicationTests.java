package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.Entities.Article;
import com.example.demo.Exceptions.EntityAlreadyExistException;
import com.example.demo.Exceptions.EntityNotFoundException;
import com.example.demo.Repository.ArticleRepository;
import com.example.demo.Service.ArticleSericeImpl;

@SpringBootTest
class TestSpring1ApplicationTests {

	@Mock
    private ArticleRepository articleRepository;

    @InjectMocks
    private ArticleSericeImpl articleService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetAllArticles() {
        // Arrange
        List<Article> mockArticles = new ArrayList<>();
        when(articleRepository.findAll()).thenReturn(mockArticles);

        // Act
        List<Article> result = articleService.getAllArticles();

        // Assert
        assertEquals(mockArticles, result);
        verify(articleRepository).findAll();
    }

    @Test
    void testGetArticleById() throws EntityNotFoundException {
        // Arrange
        String articleId = "123";
        Article mockArticle = new Article();
        when(articleRepository.findById(articleId)).thenReturn(Optional.of(mockArticle));

        // Act
        Article result = articleService.getArticleById(articleId);

        // Assert
        assertEquals(mockArticle, result);
        verify(articleRepository).findById(articleId);
    }

    @Test
    void testGetArticleById_EntityNotFoundException() {
        // Arrange
        String articleId = "123";
        when(articleRepository.findById(articleId)).thenReturn(Optional.empty());

        // Act and Assert
        assertThrows(EntityNotFoundException.class, () -> articleService.getArticleById(articleId));
        verify(articleRepository).findById(articleId);
    }

    @Test
    void testSaveArticle() throws EntityAlreadyExistException {
        // Arrange
        Article mockArticle = new Article();
        when(articleRepository.save(mockArticle)).thenReturn(mockArticle);

        // Act
        Article result = articleService.saveArticle(mockArticle);

        // Assert
        assertEquals(mockArticle, result);
        verify(articleRepository).save(mockArticle);
    }

    @Test
    void testDeleteArticle() throws EntityNotFoundException {
        // Arrange
        String articleId = "123";
        when(articleRepository.findArticleById(articleId)).thenReturn(new Article());

        // Act
        assertDoesNotThrow(() -> articleService.deleteArticle(articleId));

        // Assert
        verify(articleRepository).deleteById(articleId);
    }

    @Test
    void testDeleteArticle_EntityNotFoundException() {
        // Arrange
        String articleId = "123";
        when(articleRepository.findArticleById(articleId)).thenReturn(null);

        // Act and Assert
        assertThrows(EntityNotFoundException.class, () -> articleService.deleteArticle(articleId));
        verify(articleRepository, never()).deleteById(articleId);
    }

    @Test
    void testUpdateArticle() throws EntityNotFoundException {
        // Arrange
        String articleId = "123";
        Article existingArticle = new Article();
        Article updatedArticle = new Article();
        when(articleRepository.findById(articleId)).thenReturn(Optional.of(existingArticle));
        when(articleRepository.save(existingArticle)).thenReturn(updatedArticle);

        // Act
        Article result = articleService.UpdateArticle(articleId, updatedArticle);

        // Assert
        assertEquals(updatedArticle, result);
        verify(articleRepository).findById(articleId);
        verify(articleRepository).save(existingArticle);
    }

    @Test
    void testUpdateArticle_EntityNotFoundException() {
        // Arrange
        String articleId = "123";
        when(articleRepository.findById(articleId)).thenReturn(Optional.empty());

        // Act and Assert
        assertThrows(EntityNotFoundException.class, () -> articleService.UpdateArticle(articleId, new Article()));
        verify(articleRepository, never()).save(any());
    }
}
