package com.example.auth.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.auth.model.IntrospectResponse;
import com.example.auth.model.LoginRequest;
import com.example.auth.model.LoginResponse;
import com.example.auth.model.Response;
import com.example.auth.model.TokenRequest;
import com.example.auth.service.LoginService;
@CrossOrigin
@RestController
@RequestMapping("/auth")
public class LoginController {
	
	
	@Autowired
	LoginService loginservice;
	
	@PostMapping("/login")
	public ResponseEntity<LoginResponse> login (@RequestBody LoginRequest loginrequest) {
		return loginservice.login(loginrequest);
		}
	
	@PostMapping("/logout")
	public ResponseEntity<Response> logout (@RequestBody TokenRequest token) {
		return loginservice.logout(token);
	}
	@PostMapping("/introspect")
	public ResponseEntity<IntrospectResponse> introspect(@RequestBody TokenRequest token) {
		return loginservice.introspect(token);
	}
}
