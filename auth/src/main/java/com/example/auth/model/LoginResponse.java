package com.example.auth.model;

public class LoginResponse {

	private String access_token;
	public LoginResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	public LoginResponse(String access_token, String refresh_token, String expiry_in, String refresh_expiry_in,
			String token_type) {
		super();
		this.access_token = access_token;
		this.refresh_token = refresh_token;
		this.expiry_in = expiry_in;
		this.refresh_expiry_in = refresh_expiry_in;
		this.token_type = token_type;
	}
	public String getAccess_token() {
		return access_token;
	}
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	public String getRefresh_token() {
		return refresh_token;
	}
	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}
	public String getExpiry_in() {
		return expiry_in;
	}
	public void setExpiry_in(String expiry_in) {
		this.expiry_in = expiry_in;
	}
	public String getRefresh_expiry_in() {
		return refresh_expiry_in;
	}
	public void setRefresh_expiry_in(String refresh_expiry_in) {
		this.refresh_expiry_in = refresh_expiry_in;
	}
	public String getToken_type() {
		return token_type;
	}
	public void setToken_type(String token_type) {
		this.token_type = token_type;
	}
	private String refresh_token;
	private String expiry_in;
	private String refresh_expiry_in;
	private String token_type;
}
