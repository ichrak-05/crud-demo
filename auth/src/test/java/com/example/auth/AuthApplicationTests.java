package com.example.auth;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.example.auth.model.IntrospectResponse;
import com.example.auth.model.LoginRequest;
import com.example.auth.model.LoginResponse;
import com.example.auth.model.Response;
import com.example.auth.model.TokenRequest;
import com.example.auth.service.LoginService;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AuthApplicationTests {

	 @InjectMocks
	    private LoginService loginService;

	    @Mock
	    private RestTemplate restTemplate;

	    @Test
	    public void testLoginSuccess() {
	        // Create a mock LoginRequest object
	        LoginRequest loginRequest = new LoginRequest();
	        loginRequest.setUsername("user1");
	        loginRequest.setPassword("user1");

	        // Mock the restTemplate behavior
	        LoginResponse mockResponse = new LoginResponse(/* mock response data */);
	        Mockito.when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(), Mockito.eq(LoginResponse.class)))
	               .thenReturn(new ResponseEntity<>(mockResponse, HttpStatus.OK));

	        // Call the service method
	        ResponseEntity<LoginResponse> response = loginService.login(loginRequest);

	        // Assertions
	        assertEquals(HttpStatus.OK, response.getStatusCode());
	        // Add more assertions based on your mock response
	    }

	    
	    @Test
	    public void testLogoutSuccess() {
	        // Create a mock TokenRequest object
	        TokenRequest tokenRequest = new TokenRequest();
	        tokenRequest.setToken("mock-refresh-token");

	        // Mock the restTemplate behavior
	        ResponseEntity<Response> mockResponseEntity = ResponseEntity.ok(new Response());
	        Mockito.when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(), Mockito.eq(Response.class)))
	               .thenReturn(mockResponseEntity);

	        // Call the service method
	        ResponseEntity<Response> responseEntity = loginService.logout(tokenRequest);

	        // Assertions
	        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	        assertEquals("Logged out successfully", responseEntity.getBody().getMessage());
	    }

	    @Test
	    public void testIntrospectSuccess() {
	        // Create a mock TokenRequest object
	        TokenRequest tokenRequest = new TokenRequest();
	        tokenRequest.setToken("mock-token");

	        // Mock the restTemplate behavior
	        IntrospectResponse mockIntrospectResponse = new IntrospectResponse(/* mock response data */);
	        ResponseEntity<IntrospectResponse> mockResponseEntity = ResponseEntity.ok(mockIntrospectResponse);
	        Mockito.when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(), Mockito.eq(IntrospectResponse.class)))
	               .thenReturn(mockResponseEntity);

	        // Call the service method
	        ResponseEntity<IntrospectResponse> responseEntity = loginService.introspect(tokenRequest);

	        // Assertions
	        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	        // Add more assertions based on your mock introspect response
	    }

	    

}
