// articleservice.ts

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Article } from './Article';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  private apiUrl = 'http://localhost:8081/articles'; 

  constructor(private http: HttpClient) { }

  // Fetch all articles
  getArticles(): Observable<Article[]> {
    return this.http.get<Article[]>(this.apiUrl+'/all');
  }

  // Fetch a single article by ID
  getArticleById(articleId: string): Observable<any> {
    const url = `${this.apiUrl}/${articleId}`;
    return this.http.get<any>(url);
  }

  // Add a new article
  addArticle(article: Article): Observable<Article> {
    return this.http.post<Article>(this.apiUrl+'/add', article);
  }




  // Update an existing article
updateArticle(articleId: string, article?: Article): Observable<Article> {
  if (!articleId) {
    // Handle the case where articleId is undefined
    throw new Error('Article ID is undefined');
  }

  return this.http.put<Article>(`${this.apiUrl}/update/${articleId}`, article);
}





  GetArticleById(id:string) :  Observable<Article> {

    return this.http.get<Article>(`${this.apiUrl}/byid/${id}`)
  }

  // Delete an article by ID
  deleteArticle(articleId: string): Observable<void> {
    const url = `${this.apiUrl}/delete/${articleId}`;
    return this.http.delete<void>(url);
  }
  
}

