export interface Article {
    id: string;
    name: string;
    description: string;
    isAvailable: boolean;
    inStock: number;
  }