import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArticleService } from '../article.service'; 

@Component({
  selector: 'app-allarticles',
  templateUrl: './allarticles.component.html',
  styleUrls: ['./allarticles.component.css']
})
export class ALLArticlesComponent implements OnInit {
  articles: any[] = [];


  constructor(private service: ArticleService, private router: Router) { }

  ngOnInit(): void {
    // Subscribe to the observable returned by getArticles
    this.service.getArticles().subscribe(data => {
      this.articles = data;
    });
  }
                                          
  deleteArticle(id: string) {
    console.log(id); // Check the value in the console
    this.service.deleteArticle(id).subscribe(() => {
      this.articles = this.articles.filter(article => article.id !== id);
    });
  }
  
  updateArticle(id: number) {
    this.router.navigate(['/update', id]);
  }
}
