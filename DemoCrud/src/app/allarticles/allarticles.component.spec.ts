import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ALLArticlesComponent } from './allarticles.component';

describe('ALLArticlesComponent', () => {
  let component: ALLArticlesComponent;
  let fixture: ComponentFixture<ALLArticlesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ALLArticlesComponent]
    });
    fixture = TestBed.createComponent(ALLArticlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
