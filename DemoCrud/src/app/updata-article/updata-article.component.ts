import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticleService } from '../article.service';
import { Article } from '../Article';

@Component({
  selector: 'app-updata-article',
  templateUrl: './updata-article.component.html',
  styleUrls: ['./updata-article.component.css']
})
export class UpdataArticleComponent implements OnInit {

  article?: Article;
  data: any;

  constructor(private service: ArticleService, private route: ActivatedRoute, private router: Router) { }

  form = new FormGroup({
    name: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    isAvailable: new FormControl(false, [Validators.required]),
    inStock: new FormControl('', [Validators.required, Validators.pattern(/^-?\d+\.?\d*$/)])
  });

  ngOnInit(): void {
    let id = this.route.snapshot.params['id'];

    this.service.GetArticleById(id).subscribe(data => {
      this.form.patchValue({
        name: data.name,
        description: data.description,
        isAvailable: data.isAvailable,
        inStock: String(data.inStock) // Convert to string
      });

      this.article = data;
      console.log(this.article);
    });
  }

 submit() {
  this.data = this.form.value;
  console.log(this.data);

  if (this.article && this.article.id) {
    this.service.updateArticle(this.article.id, this.data).subscribe(data => {
      console.log(data);
    });

    this.router.navigate(['/all']);
  } else {
    console.error('Article or Article ID is undefined');
  }
}

}
