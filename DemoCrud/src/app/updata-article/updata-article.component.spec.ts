import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdataArticleComponent } from './updata-article.component';

describe('UpdataArticleComponent', () => {
  let component: UpdataArticleComponent;
  let fixture: ComponentFixture<UpdataArticleComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UpdataArticleComponent]
    });
    fixture = TestBed.createComponent(UpdataArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
