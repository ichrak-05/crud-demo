import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewArticleComponent } from './new-article/new-article.component';
import { UpdataArticleComponent } from './updata-article/updata-article.component';
import { ALLArticlesComponent } from './allarticles/allarticles.component';
import { LoginComponent } from './login/login.component';
const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'new', component: NewArticleComponent },
  { path: 'all', component: ALLArticlesComponent },
  { path: 'update/:id', component: UpdataArticleComponent },
  { path: 'login', component: LoginComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
