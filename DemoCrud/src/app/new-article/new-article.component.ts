import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router'; // Import Router
import { ArticleService } from '../article.service';

@Component({
  selector: 'app-new-article',
  templateUrl: './new-article.component.html',
  styleUrls: ['./new-article.component.css']
})
export class NewArticleComponent implements OnInit {
  articleForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private articleService: ArticleService, private router: Router) { }

  ngOnInit(): void {
    this.articleForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      isAvailable: [false, Validators.required],
      inStock: [null, Validators.required],
    });
  }

  submit() {
    const formData = this.articleForm.value;

    // Ensure inStock is set to 0 if it's currently null
    formData.inStock = formData.inStock || 0;

    this.articleService.addArticle(formData).subscribe(data => {
      console.log(data);
      this.router.navigate(['/all']);
    });
  }
}
